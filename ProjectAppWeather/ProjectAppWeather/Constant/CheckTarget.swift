//
//  CheckTarget.swift
//  ProjectAppWeather
//
//  Created by MT382 on 3/13/19.
//  Copyright © 2019 Trungdn. All rights reserved.
//

import Foundation

var baseUrl = ""

struct CheckTarget {
    
    func check() {
        #if DEVELOPMENT
        baseUrl = urlDev
        //        let imageBaseUrl = "http://14.160.64.50:21021/"
        print("development")
        #else
        baseUrl = urlPro
        //        let imageBaseUrl = "http://192.168.50.132:8989/"
        print("production")
        #endif
    }
}
