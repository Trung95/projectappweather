//
//  BaseViewController.swift
//  ProjectAppWeather
//
//  Created by MT382 on 3/13/19.
//  Copyright © 2019 Trungdn. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var backGroundImageView = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViewBase()

    }
    
    func setUpViewBase() {
        
        let screenSize: CGRect = UIScreen.main.bounds
        backGroundImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
        view.addSubview(backGroundImageView)
        view.sendSubviewToBack(backGroundImageView)
        backGroundImageView.image = UIImage(named: "BackGround")
        backGroundImageView.contentMode = .scaleAspectFill

    }

}
